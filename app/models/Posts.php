<?php


class Posts extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $government_agency_id;
    public $category_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("government_agency_id", "GovernmentAgencies", "id");
        $this->belongsTo("category_id", "Categories", "id");
        $this->hasMany("id", "Comments", "post_id");
        $this->hasMany("id", "PostMoods", "post_id");
        $this->hasMany("id", "PostFiles", "post_id");
    }
	
    
}