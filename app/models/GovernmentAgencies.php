<?php


class GovernmentAgencies extends \Phalcon\Mvc\Model {
	
	public $id;

    public $abbreviation;

	public function initialize()
    {
        //$this->hasMany("id", "Reports", "government_agency_id");
        $this->hasMany("id", "Posts", "government_agency_id");
    }
    
}