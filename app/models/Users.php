<?php


class Users extends \Phalcon\Mvc\Model {
	
	public $id;

    public $username;

    public function initialize()
    {
        $this->hasMany("id", "Reports", "user_id");
        $this->hasMany("id", "Posts", "user_id");
    }
    
}