<?php


class CommentMoods extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $comment_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("comment_id", "Comments", "id");
    }
	
    
}