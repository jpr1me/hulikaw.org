<?php


class ReportEvidenceFiles extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $report_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("report_id", "Reports", "id");
    }
	
    
}