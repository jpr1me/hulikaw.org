<?php

class PostFiles extends \Phalcon\Mvc\Model {
    
    public $id;

    public $user_id;

    public $post_id;

    public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("post_id", "Posts", "id");
    }
    
    
}