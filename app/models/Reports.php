<?php


class Reports extends \Phalcon\Mvc\Model {
	
	public $id;

    public $user_id;

    public $government_agency_id;
    public $category_id;

	public function initialize()
    {
        $this->belongsTo("user_id", "Users", "id");
        $this->belongsTo("government_agency_id", "GovernmentAgencies", "id");
        $this->hasMany("id", "ReportEvidenceFiles", "report_id");
        $this->hasMany("id", "Comments", "report_id");
    }
	
    
}