<?php

    $di->set('router', function(){
        
    	$router = new \Phalcon\Mvc\Router();

        //Define a route
        $router->add(
                '/c={category}',
            array(
                "controller"     => "index",
                "action"         => "index"
            )
        );

        $router->add(
                '/a={agency}',
            array(
                "controller"     => "index",
                "action"         => "index"
            )
        );

        $router->add(
                '/c={category}/a={agency}',
            array(
                "controller"     => "index",
                "action"         => "index",
            )
        );
        // ->convert('category', function($category) {
        // //Transform the slug removing the dashes
        // return str_replace('-', '', $category);
        // })
        // ->convert('agency', function($agency) {
        // //Transform the slug removing the dashes
        // return str_replace('-', '', $agency);
        // });

        $router->add(
            '/login',
            array(
                'controller' => 'users',
                'action'     => 'login'
            )
        );

        $router->add(
            '/logout',
            array(
                'controller' => 'users',
                'action'     => 'logout'
            )
        );

        $router->add(
            '/signup',
            array(
                'controller' => 'users',
                'action'     => 'signup'
            )
        );

        $router->add(
            '/register',
            array(
                'controller' => 'users',
                'action'     => 'signup'
            )
        );

    
        
        return $router;
    });