<?php

class CommentMoodsController extends ControllerBase
{


	public function initialize()
	{		
		//$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
	}

    public function indexAction()
    {

    }

    public function createAction()
    {
		// Check whether the request was made with method POST
    	if ($this->request->isPost() == true) {
    		if ($this->request->isAjax() == true) {
    			$this->view->disable();
		     	$sessionUser = $this->session->get('sessionUser');

				$commentMood = new CommentMoods();
				$commentMood->created = date('Y-m-d H:i:s');
				$commentMood->modified = date('Y-m-d H:i:s');
				$commentMood->user_id = $sessionUser['id'];
				$commentMood->comment_id = $this->request->getPost('id');
				$commentMood->mood = $this->request->getPost('mood');  
				
				if($commentMood->create()){
					// Query robots binding parameters with string placeholders
					$conditions = "comment_id = :comment_id: AND mood = :mood:";

					//Parameters whose keys are the same as placeholders
					$parameters = array(
					    "comment_id" => $this->request->getPost('id'),
					    "mood" => $this->request->getPost('mood')
					);

					//Perform the query
					$mood = CommentMoods::find(array(
					    $conditions,
					    "bind" => $parameters
					));
					$data = array('result' => true, 'mood_count' => count($mood));
				} else {
					$data = array('result' => false);
				}
				//echo json_encode($data);
			}
		}
    }

}
