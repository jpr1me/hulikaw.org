<?php

class PostMoodsController extends ControllerBase
{


	public function initialize()
	{		
		//$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
	}

    public function indexAction()
    {

    }

     public function createAction()
    {
		// Check whether the request was made with method POST
    	if ($this->request->isPost() == true) {
    		if ($this->request->isAjax() == true) {
    			$this->view->disable();
		     	$sessionUser = $this->session->get('sessionUser');

				$postMood = new PostMoods();
				$postMood->created = date('Y-m-d H:i:s');
				$postMood->modified = date('Y-m-d H:i:s');
				$postMood->user_id = $sessionUser['id'];
				$postMood->post_id = $this->request->getPost('id');
				$postMood->mood = $this->request->getPost('mood');
				$post = Posts::findFirstById($this->request->getPost('id'));  
				if($postMood->save()){
					$notif = new Notifications();
					$notif->created = date('Y-m-d H:i:s');
					$notif->modified = date('Y-m-d H:i:s');
					$notif->user_id = $sessionUser['id'];
					$notif->recipient_id = $post->user_id;
					$notif->message = $sessionUser['username'].' '.$this->request->getPost('mood').' mood on your post.';
					$notif->link = 'soon'; 
					$notif->action_id = 'p-emoticons-'.$this->request->getPost('id');
					$notif->type = 'mood';
					$notif->opened = 'no'; 
					$notif->save();
					// Query robots binding parameters with string placeholders
					$conditions = "post_id = :post_id: AND mood = :mood:";

					//Parameters whose keys are the same as placeholders
					$parameters = array(
					    "post_id" => $this->request->getPost('id'),
					    "mood" => $this->request->getPost('mood')
					);

					//Perform the query
					$mood = PostMoods::find(array(
					    $conditions,
					    "bind" => $parameters
					));
					$data = array('result' => true, 'mood_count' => count($mood));
				} else {
					$data = array('result' => false);
				}
				echo json_encode($data);
			}
		}
    }

}
