<?php

class IndexController extends ControllerBase
{


	public function initialize()
	{		
		$this->view->setTemplateAfter('default');

		
        if(isset($_COOKIE['hku']) && isset($_COOKIE['hkuid']) && isset($_COOKIE['hktoken'])) {
            $user = Users::findFirstByUsername($_COOKIE['hku']);
            if($user){
                if($_COOKIE['hkuid'] == $user->id && $_COOKIE['hktoken'] == $user->token){
                    $this->session->set('sessionUser', array('id' => $user->id,
                                                             'username' => $user->username));
                }
            }
        }
        $this->view->setVar('sessionUser', $this->session->get('sessionUser'));
		if(!$this->session->get('sessionUser')){
			$this->response->redirect('login');
		}

        $sessionUser = $this->session->get('sessionUser');
        $conditions = "recipient_id = :recipient_id: AND opened = :opened:";
        $parameters = array(
            "recipient_id" => $sessionUser['id'],
            "opened" => 'no'
        );
        $notifications = Notifications::find([
                            $conditions,
                            'bind' => $parameters,
                            'order' => 'id DESC']);
        $this->view->setVar('notifications', $notifications);
	}

    public function indexAction()
    {
        $category = $this->dispatcher->getParam("category");
        $this->view->setVar('category', $category);
        $agency = $this->dispatcher->getParam("agency");
        
    	if(!empty($category) && empty($agency)){
            $category = Categories::findFirstByCategory($category);;
            $posts = Posts::find(['category_id = "'.$category->id.'"', 'order' => 'id DESC']);
        } else
        if(empty($category) && !empty($agency)){
            $agency = GovernmentAgencies::findFirstByName($agency);
            $posts = Posts::find(['government_agency_id = '.$agency->id, 'order' => 'id DESC']);
        } else
        if(!empty($category) && !empty($agency)){
            $category = Categories::findFirstByCategory($category);
            $agency = GovernmentAgencies::findFirstByName($agency);
            // Query robots binding parameters with string placeholders
            $conditions = "category_id = :category_id: AND government_agency_id = :government_agency_id:";

            //Parameters whose keys are the same as placeholders
            $parameters = array(
                "category_id" => $category->id,
                "government_agency_id" => $agency->id
            );
            $posts = Posts::find([
                                $conditions,
                                'bind' => $parameters,
                                'order' => 'id DESC']);
        } else {
            $posts = Posts::find(['order' => 'id DESC']);
        }

        //$reports = Reports::find();
        $this->view->setVar('posts', $posts);

        $governmentAgencies = GovernmentAgencies::find();
    	$this->view->setVar('governmentAgencies', $governmentAgencies);
    	$categories = Categories::find();
    	$this->view->setVar('categories', $categories);
	
    }

    public function newnotificationsAction()
    {
        // $sessionUser = $this->session->get('sessionUser');

        // $conditions = "recipient_id = :recipient_id: AND opened = :opened:";

     //    //Parameters whose keys are the same as placeholders
     //    $parameters = array(
     //        "recipient_id" => $sessionUser['id'],
     //        "opened" => false
     //    );
     //    $notifications = Notifications::find([
     //                        $conditions,
     //                        'bind' => $parameters,
     //                        'order' => 'id DESC']);
        return "Whats up!";
    }

    // public function searchAction() {
    //     if ($request->isPost() == true) {
    //         $searchData = $this->request->getPost('search');

    //         $category = Categories::find(array(
    //                                 "category LIKE = '".$searchData."%'",
    //                             ));

    //         $agency = GovernmentAgencies::find(array(
    //                                 "name LIKE = '".$searchData."%'",
    //                             ));
            
    //         $conditions = "category_id = :category_id: OR government_agency_id = :government_agency_id: OR post = :post:";

    //         //Parameters whose keys are the same as placeholders
    //         $parameters = array(
    //             "category_id" => $category->id,
    //             "government_agency_id" => $agency->id
    //         );
    //         $posts = Posts::find([
    //                             $conditions,
    //                             'bind' => $parameters,
    //                             'order' => 'id DESC']);

    //     }
    // }

}
