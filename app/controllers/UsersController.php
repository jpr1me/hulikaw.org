<?php 

use \Phalcon\Tag;

class UsersController extends ControllerBase
{

	public function initialize()
	{		
		$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
	}

	public function indexAction()
	{
		//$user = $this->session->get('sessionUser');
		//$this->view->setVars(array('user'=>Users::findFirstById($user->id)));

	}

	public function loginAction()
	{

		if ($this->request->isPost()) {

			// if($this->security->checkToken() == false){
			// 	$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
			// 	return $this->response->redirect('login');
			// }
			//$this->view->disable();
			$username = $this->request->getPost('username'); // $_POST
			$password = $this->request->getPost('password');

			if(empty($username) || empty($password)){ 
				$this->flash->warning('<button type="button" class="close" data-dismiss="alert">×</button>All fields required');
				return $this->response->redirect('login'); 
			}

			$user = Users::findFirstByUsername($username);
			if ($user == true && $this->security->checkHash($password, $user->password)) {
					$this->session->set('sessionUser', array('id' => $user->id,
															 'username' => $user->username));
					
					$number_of_days = 90 ;
					$date_of_expiry = time() + 60 * 60 * 24 * $number_of_days ;
					$token = $this->security->hash($password);
					setcookie('hku',$username,$date_of_expiry);
					setcookie('hkuid',$user->id,$date_of_expiry);
					setcookie('hktoken',$token,$date_of_expiry);

					
					$user->modified = date('Y-m-d H:i:s');
					$user->token = $token;
					
					if($user->update()){
						$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>You are now logged in.');
						$this->response->redirect('');
					}

			} else {
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Incorrect username or password.');
				$this->response->redirect('login');
			}
		}	
		
		//$this->response->redirect('users');
	}

	public function logoutAction(){
		$this->view->disable();
		if($this->session->destroy('sessionUser')){
			$date_of_expiry = time() - 60 ;
			setcookie( "hku", "anonymous", $date_of_expiry);
			setcookie( "hkuid", "anonymous", $date_of_expiry);
			setcookie( "hktoken", "anonymous", $date_of_expiry);
			$this->flash->success('You are now logged out.');
			$this->response->redirect('');
		}
	}


	public function signupAction()
	{
		if ($this->request->isPost()) {
			
			$error = 0;
			// if($this->security->checkToken() == false){
			// 	$error = 1;
			// 	$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Invalid CSRF Token');
			// 	return $this->response->redirect('signup');
			// }
			$username = $this->request->getPost('username'); 
			$password = $this->request->getPost('password');
			$confirmPassword = $this->request->getPost('confirm_password');
			$email = $this->request->getPost('email', 'email');
			$mobile = $this->request->getPost('mobile');
			

						
			if(empty($username) || empty($password)){ 
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Username and password are required.');
				return; 
			}

			if($password != $confirmPassword){ 
				$errorMsg = "Confirm password does not match"; 
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
				return; 
			}

			if(Users::findFirstByUsername($username)){ 
				$errorMsg = "Username is already in use. Please try again.";
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
				return; 
			}

			if(!empty($email) && UserContacts::findFirstByContact($email)){
				$errorMsg = "Email is already in use. Please try again.";
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
				return ; 
			}
			if(!empty($mobile) && UserContacts::findFirstByContact($mobile)){
				$errorMsg = "Mobile is already in use. Please try again.";
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>'.$errorMsg);
				return; 
			} 

			$user = new Users();
			$user->created = date('Y-m-d H:i:s');
			$user->modified = date('Y-m-d H:i:s');
			$user->username = $username;
			$user->password = $this->security->hash($password);
			$user->group_id = 4;
			
			if($user->create()){	
				$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Thank you! You are now registered.');
				return $this->response->redirect('index');
			} else {
				//print_r($user->getMessages());
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Registration failed. Please try again.');
				//return $this->response->redirect('signup'); 
			}
		}	
	}

	public function updateAction()
	{
		
		if(!$user = Users::findFirstById(6))
		{
			echo "Users does not exist";
			die;
		}

		$user->email = 'PhalconPHPNinja';
		$result = $user->update();
		if(!$result)
		{
			print_r($user->getMessages());
		}


	}

	public function deleteAction()
	{
		
		if(!$user = Users::findFirstById(2))
		{
			echo "Users does not exist";
			die;
		}

		$result = $user->delete();
		if(!$result)
		{
			print_r($user->getMessages());
		}

	}


	public function uAction($username = null)
	{
		
		if(!$user = Users::findFirstByUsername($username))
		{
			$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>User does not exist.');
			$this->response->redirect('index');
		}

		

	}


} 
