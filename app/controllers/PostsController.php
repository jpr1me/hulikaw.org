<?php

class PostsController extends ControllerBase
{


	public function initialize()
	{		
		$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
		// if(!$this->session->get('sessionUser')){
		// 	$this->response->redirect('login');
		// }
	}

    public function indexAction()
    {

    }

    public function createAction()
    {
		// Check whether the request was made with method POST
    	if ($this->request->isPost() == true) {
	     	$sessionUser = $this->session->get('sessionUser');
	     	if(empty($this->request->getPost('content')) || empty($this->request->getPost('category_id')) || empty($this->request->getPost('government_agency_id')) ) {
	     		$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Required fields empty. Please try again.');
				return $this->response->redirect('index');
	     	}

			$post = new Posts();
			$post->created = date('Y-m-d H:i:s');
			$post->modified = date('Y-m-d H:i:s');
			$post->user_id = $sessionUser['id'];
			$post->category_id = $this->request->getPost('category_id');
			$post->government_agency_id = $this->request->getPost('government_agency_id');
			$post->content = $this->request->getPost('content');  
			$post->post_type = 'Post';

			if($post->create()){
				$postId = $post->id;
				if($this->request->hasFiles() == true){
					$uploads = $this->request->getUploadedFiles();
					$this->uploadFile($postId, $sessionUser['id'], $uploads);
				}	
				$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Successful.');
				return $this->response->redirect('index');
			} else {
				//print_r($user->getMessages());
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Failed. Please try again.');
				//return $this->response->redirect('signup'); 
			}
		}
    }

    public function create_reportAction()
    {
		// Check whether the request was made with method POST
    	if ($this->request->isPost() == true) {
	     	$sessionUser = $this->session->get('sessionUser');

			if(empty($this->request->getPost('first_name')) || empty($this->request->getPost('middle_name')) || empty($this->request->getPost('last_name'))
				|| empty($this->request->getPost('government_agency_id')) || empty($this->request->getPost('designation')) || empty($this->request->getPost('asset_type')) 
				|| empty($this->request->getPost('estimated_value')) || empty($this->request->getPost('content')) ) {
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Required fields empty. Please try again.');
				return $this->response->redirect('index');
			}

			//Generate report code
			$continue = true;
			do {
				$reportCode  = trim($this->generateCode(5, 5));
				//Check if generated code is already used
				$codeCheck = Posts::findFirstByReportCode($reportCode);
				if ($codeCheck == false){
					$continue = false;
				}
			} while ($continue) ;

			$post = new Posts();
			$post->created = date('Y-m-d H:i:s');
			$post->modified = date('Y-m-d H:i:s');
			$post->user_id = $sessionUser['id'];
			$post->report_code = $reportCode;
			$post->category_id = 1;
			$post->government_agency_id = $this->request->getPost('government_agency_id');
			$post->content = $this->request->getPost('content'); 
			$post->post_type = 'Report';
			$post->first_name = $this->request->getPost('first_name'); 
			$post->middle_name = $this->request->getPost('middle_name');
			$post->last_name = $this->request->getPost('last_name');
			$post->suffix = $this->request->getPost('suffix'); 
			$post->designation = $this->request->getPost('designation');
			$post->asset_type = $this->request->getPost('asset_type');
			$post->estimated_value = $this->request->getPost('estimated_value');
			$post->report_status = 'Processing'; 

			if($post->create()){
				$postId = $post->id;
				if($this->request->hasFiles() == true){
					$uploads = $this->request->getUploadedFiles();
					$this->uploadFile($postId, $sessionUser['id'], $uploads);
				}	
				$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Successful.');
				return $this->response->redirect('index');
			} else {
				//print_r($user->getMessages());
				$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Failed. Please try again.');
				//return $this->response->redirect('signup'); 
			}
		}
    }

    function uploadFile($postId = null, $userId = null, $uploads = null){
    	error_log('This is uploaded function');
		ini_set('upload_max_filesize', '64M');
		set_time_limit(900);
		$fileName = null;
		$newFileName = null;
		// $dateTaken = $_GET['date_taken'];
		// $locationTaken = $_GET['location_taken'];
		//$content = $_GET['content'];
		$isVideo = false;    	

		$isUploaded = false;
		#do a loop to handle each file individually
		foreach($uploads as $upload){
			#define a “unique” name and a path to where our file must go
			$fileName = $upload->getname();
			$newFileName = md5(uniqid(rand(), true)).'-'.strtolower($fileName);
			$fileInfo = new SplFileInfo($fileName);
			$fileExt = $fileInfo->getExtension();
			//$fileExt = $upload->getExtension();
			$fileImageExt = array('jpeg', 'jpg', 'png');
			//error_log("File Extension :".$fileExt, 0);
			$fileType = '';
			$filePath = '';
			$path = '/var/www/HuliKawFiles/'.$newFileName;
			if(in_array($fileExt, $fileImageExt)){
				$path = '/var/www/HuliKawFiles/image/'.$newFileName;
				$filePath = 'image/'.$newFileName;
				$fileType = 'Image';
			} 
			$fileVideoExt = array('flv', 'mp4', '3gp', 'avi', 'ogg', 'mov', 'wmv', 'm4v', 'svi', '3g2');
			if(in_array($fileExt, $fileVideoExt)){
				$isVideo = true;
				$path = '/var/www/HuliKawFiles/video/'.$newFileName;
				$filePath = 'video/'.$newFileName;
				$fileType = 'Video';
			}
			#move the file and simultaneously check if everything was ok
			($upload->moveTo($path)) ? $isUploaded = true : $isUploaded = false;
		}

		#if any file couldn't be moved, then throw an message
		if($isUploaded) {
			$thumbNail = '';
			if($isVideo){
				// where ffmpeg is located, such as /usr/sbin/ffmpeg
				$ffmpeg = 'ffmpeg';
				 
				// the input video file
				$video  = $path;
				$thumbNailName = md5(uniqid(rand(), true)).'.jpg';
				// where you'll save the image
				$thumbNailPath  = '/var/www/HuliKawFiles/video/thumbnail/'.$thumbNailName;
				$thumbNail = 'video/thumbnail/'.$thumbNailName;
				 
				// default time to get the image
				$second = 0.5;
				 
				// get the duration and a random place within that
				$cmd = "$ffmpeg -i $video 2>&1";
				if (preg_match('/Duration: ((\d+):(\d+):(\d+))/s', `$cmd`, $time)) {
				    $total = ($time[2] * 3600) + ($time[3] * 60) + $time[4];
				    $second = rand(1, ($total - 1));
				}
				// get the screenshot
				$cmd = "$ffmpeg -i $video -filter:v yadif -an -ss $second -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $thumbNailPath 2>&1";
				
				$result = shell_exec($cmd);

				//convert to mp4
				$newVideoName = md5(uniqid(rand(), true)).date('ymdhis').'.mp4';
				$newVideoPath = '/var/www/HuliKawFiles/video/'.$newVideoName;
				$cmd = "$ffmpeg -i $video -strict -2 $newVideoPath 2>&1";
				$result = shell_exec($cmd);
				//error_log($result);
				if($result) {
					unlink($path);
					$filePath = 'video/'.$newVideoName;
				}
				//error_log($result);
				//$return = `$cmd`;
			}
			
			$phql = "INSERT INTO PostFiles (created, modified, user_id, post_id, file_name, thumbnail, file_type) VALUES 
			(:created:, :modified:, :user_id:, :post_id:, :file_name:, :thumbnail:, :file_type:)";

				$evidenceData = $this->modelsManager->executeQuery($phql, array(
			    	'created' 			=> 	date('Y-m-d H:i:s'),
			    	'modified' 			=> 	date('Y-m-d H:i:s'),
			        'user_id' 			=> 	$userId,
			        'post_id' 			=> 	$postId,
			        'file_name' 		=> 	$filePath,
			        'thumbnail'			=>  $thumbNail,
			        'file_type'			=>  $fileType
			    ));

				$response = new Phalcon\Http\Response();
				$jsonResponse = array();

			     //Check if the insertion was successful
			    if ($evidenceData->success() == true) {
					return 1;
				} else {
					return 0;
				}

		} else {
			die('Something wrong. Please try again.');
		}
    }

    function generateCode($s_length, $n_length){
		$str='';
		$num='';
		$total_length = $s_length + $n_length ;
		$continue = true;
		do {

			if ($s_length) {
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
				$str = '';
				$size = strlen( $chars );
				for( $i = 0; $i < $s_length; $i++ ) {
					$str .= $chars[ rand( 0, $size - 1 ) ];
				}
				$str = strtoupper($str);
			} else {
				$str = '';
			}	

			if ($n_length) {
				$nums = "123456789";
				$num = '';
				$size = strlen($nums);
				for( $i = 0; $i < $n_length; $i++ ) {
					$num .= $nums[ rand( 0, $size - 1 ) ];
				}
				$num = strtoupper($num);
			} else {
				$num = '';
			}
	
				$gen_string = strlen($str) + strlen($num);
				
				if ($total_length == $gen_string) {
					$continue = false;
				}
		
		} while  ($continue);

		return $str.$num;
	}



}
