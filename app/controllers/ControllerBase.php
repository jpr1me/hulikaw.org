<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	public function initialize()
	{
		if(!$this->session->get('sessionUser')){
			$this->response->redirect('login');
		}
		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
	}

}
