<?php

class CommentsController extends ControllerBase
{


	public function initialize()
	{		
		//$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
		// if(!$this->session->get('sessionUser')){
		// 	$this->response->redirect('login');
		// }
	}

    public function indexAction()
    {

    }

     public function createAction()
    {
     	$sessionUser = $this->session->get('sessionUser');

		$comment = new Comments();
		$comment->created = date('Y-m-d H:i:s');
		$comment->modified = date('Y-m-d H:i:s');
		$comment->user_id = $sessionUser['id'];
		$comment->post_id = $this->request->getPost('post_id'); 
		$comment->comment = $this->request->getPost('comment'); 
		$post = Posts::findFirstById($this->request->getPost('post_id'));
		if($comment->save()){	
			$notif = new Notifications();
			$notif->created = date('Y-m-d H:i:s');
			$notif->modified = date('Y-m-d H:i:s');
			$notif->user_id = $sessionUser['id'];
			$notif->recipient_id = $post->user_id;
			$notif->message = $sessionUser['username'].' comment on your post.';
			$notif->link = 'soon'; 
			$notif->action_id = 'commentContent'.$comment->id;
			$notif->type = 'comment';
			$notif->opened = 'no'; 
			$notif->save();
			$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Successful.');
			return $this->response->redirect('index');
		} else {
			//print_r($user->getMessages());
			$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Failed. Please try again.');
			return $this->response->redirect('index');
		}
		
    }

}
