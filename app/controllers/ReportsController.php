<?php

class ReportsController extends ControllerBase
{


	public function initialize()
	{		
		$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
		// if(!$this->session->get('sessionUser')){
		// 	$this->response->redirect('login');
		// }
	}

    public function indexAction()
    {

    }

     public function createAction()
    {
     	$sessionUser = $this->session->get('sessionUser');
  //   	$content = $this->request->getPost('content'); 
		// $catego = $this->request->getPost('category_id');
		// $username = $this->request->getPost('government_agency_id'); 

		$post = new Posts();
		$post->created = date('Y-m-d H:i:s');
		$post->modified = date('Y-m-d H:i:s');
		$post->user_id = $sessionUser['id'];
		$post->category_id = $this->request->getPost('category_id');
		$post->government_agency_id = $this->request->getPost('government_agency_id');
		$post->content = $this->request->getPost('content');  
		
		if($post->create()){	
			$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Successful.');
			return $this->response->redirect('index');
		} else {
			//print_r($user->getMessages());
			$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Failed. Please try again.');
			//return $this->response->redirect('signup'); 
		}
		
    }

}
