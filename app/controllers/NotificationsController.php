<?php

class NotificationsController extends ControllerBase
{


	public function initialize()
	{		
		//$this->view->setTemplateAfter('default');

		$this->view->setVar('sessionUser', $this->session->get('sessionUser'));
		// if(!$this->session->get('sessionUser')){
		// 	$this->response->redirect('login');
		// }
	}

  //   public function indexAction()
  //   {

  //   }

  //   public function createAction()
  //   {
  //    	$sessionUser = $this->session->get('sessionUser');

		// $comment = new Comments();
		// $comment->created = date('Y-m-d H:i:s');
		// $comment->modified = date('Y-m-d H:i:s');
		// $comment->user_id = $sessionUser['id'];
		// $comment->post_id = $this->request->getPost('post_id'); 
		// $comment->comment = $this->request->getPost('comment'); 
		
		// if($comment->create()){	
		// 	$this->flash->success('<button type="button" class="close" data-dismiss="alert">×</button>Successful.');
		// 	return $this->response->redirect('index');
		// } else {
		// 	//print_r($user->getMessages());
		// 	$this->flash->error('<button type="button" class="close" data-dismiss="alert">×</button>Failed. Please try again.');
		// 	return $this->response->redirect('index');
		// }
		
  //   }

    public function notificationsAction()
    {
    	$sessionUser = $this->session->get('sessionUser');

    	$conditions = "recipient_id = :recipient_id: AND opened = :opened:";

        //Parameters whose keys are the same as placeholders
        $parameters = array(
            "recipient_id" => $sessionUser['id'],
            "opened" => false
        );
        $notifications = Notifications::find([
                            $conditions,
                            'bind' => $parameters,
                            'order' => 'id DESC']);
        return "Whats up!";
    }

    public function notifOpenedAction()
    {
    	$notif = Notifications::findFirstById($this->request->getPost('notification_id'));
		$notif->opened = 'yes';
		$notif->update();
    }

}
